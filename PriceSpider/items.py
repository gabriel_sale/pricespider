# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class BuyBox(scrapy.Item):
    ean = scrapy.Field()
    sku = scrapy.Field()
    preco_por = scrapy.Field()
    parcelas = scrapy.Field()
    valor_parcela = scrapy.Field()
    taxa_juros = scrapy.Field()
    vendendor = scrapy.Field()
    ranking = scrapy.Field()
    origem = scrapy.Field()


class Product(scrapy.Item):
    ean = scrapy.Field()
    sku = scrapy.Field()
    nome_loja = scrapy.Field()
    departamento = scrapy.Field()
    categoria = scrapy.Field()
    url = scrapy.Field()
    imagem = scrapy.Field()
    preco_de = scrapy.Field()
    preco_por = scrapy.Field()
    preco_boleto = scrapy.Field()
    preco_prazo = scrapy.Field()
    parcelas = scrapy.Field()
    valor_parcela = scrapy.Field()
    taxa_juros = scrapy.Field()
    vendendor = scrapy.Field()
    origem = scrapy.Field()
    buybox_list = scrapy.Field()


class Frete(scrapy.Item):
    tipo_entrega = scrapy.Field()
    valor_frete = scrapy.Field()
    prazo_entrega = scrapy.Field()
