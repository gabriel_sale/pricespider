import scrapy


class QuotesSpider(scrapy.Spider):
    name = "store_americanas"

    def start_requests(self):
        urls = [
            'https://www.americanas.com.br/produto/134253960/smartphone-samsung-galaxy-a10-32gb-dual-chip-android-9-0-tela-6-2-octa-core-4g-camera-13mp-preto?pfm_carac=134253960&pfm_page=search&pfm_pos=grid&pfm_type=search_page'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        page = response.url.split("/")[-2]
        filename = 'quotes-%s.html' % page
        with open(filename, 'wb') as f:
            f.write(response.body)
        self.log('Saved file %s' % filename)